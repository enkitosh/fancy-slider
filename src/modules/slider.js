/*
 *  Fancy slider
 *  author: enki
 *  Date: 14 September, 2014
 * */

define(["jquery"], function($) {

    var screen    = {};
    var graphics  = {};
    var dimension = {};
    var limit     = 0;
    
    function setGraphics() {
        limit  = 640; // TODO: Make this an editable configuration for user
        dimension = {
            big: {
                width: 640,
                height: 160
            },
            small: {
                width: 320,
                height: 80
            },
            getWidth: function() {
                return (screen.width >= 640) ? this.big.width : this.small.width;
            },
            getHeight: function() {
                return (screen.width >= 640) ? this.big.height : this.small.height;
            }
        };

        graphics = {
           button: {
               big  : 'assets/output/sliderButton@2x.png',
               small: 'assets/output/sliderButton.png',
           },
           scale: {
               big  : 'assets/output/VAS-scale@2x.png',
               small: 'assets/output/VAS-scale.png'
           },
           getButton: function() {
               return (screen.width > limit) ? this.button.big : this.button.small;
           },
           getScale: function() {
               return (screen.width > limit) ? this.scale.big : this.scale.small;
           }
        };
    }

    function reset() {
        getScreen();
        setGraphics();
        layout();
    }

    function init() {
        // Setup graphics
        getScreen();
        setGraphics();
        layout();

        // Events
        $(window).resize(function() {
            reset();
        });

        $('#slider_button').on('click', function(e) {
            var padding = parseInt($('#slider_button').css('padding-left'));
            var left = $('#slider_button').offset().left + padding + 10;
            $('#button_image').css({
                'position' : 'relative',
                'top' :0,
                'left':e.pageX - left
            });
        });

    }

    function layout() {
        $('#fancy_slider').empty();
        $('#fancy_slider').width(dimension.getWidth()).height(dimension.getHeight());
        $('#fancy_slider').css('background-image', 'url(' + graphics.getScale() + ')');
        $('#fancy_slider').css('background-repeat', 'no-repeat');
        $('#fancy_slider').prepend('<div id="slider_button"></div>');
        $('#slider_button').prepend('<img id="button_image" src="'+ graphics.getButton() + '" />');
        $('#slider_button').css('position', 'relative');
        $('#slider_button').width(dimension.getWidth() * 0.90);
        $('#slider_button').css("padding-top", dimension.getHeight() - (dimension.getHeight() * 0.22));
        $('#slider_button').css("padding-left", dimension.getWidth() - (dimension.getWidth()  * 0.9577));
    }

    function getScreen() {
        // get screen width/height
        screen = {
            width : $(window).width(),
            height: $(window).width()
        };
        
    }

    return {
        init : init
    };
});
