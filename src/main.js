require.config({
    baseUrl : '../bower_components',
    paths : {
        'jquery' : 'jquery/dist/jquery.min',
        'modules' : '../src/modules'
    },
    shim : {
        'modules/slider' : {
            deps: ["jquery"]
        }
    }
});

require(["jquery"], function($) {
    
});
