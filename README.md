Fancy slider
============

To install dependencies to test run

    bower install

Slider adjusts to browser by checking it's limit (a hardcoded value at the moment) 
of width 640px. Just change the limit variable if this should be less.

Preview use of the slider can be seen at index.html
It is very easy to use, include it like this

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div id="fancy_slider"></div>
    </body>
    </html>

TODO:
    Get position of slider in terms of the numerical values on top of slider
    Support user configuration in the init call
